/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.retofinal;
       
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author camilo
 */
public class Producto {
    //atributos
    private String nombre;
    private String id;
    private double temperatura;
    private double valorBase;
    //contructor
    public Producto(){
        
    }
    
    public Producto (String nombre, String id, double temperatura, double valorBase){
        this.nombre = nombre;
        this.id = id;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }
   
    
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public String getId(){
        return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public double getTemperatura(){
        return temperatura;
    }
    public void setTemperatura(double temperatura){
        this.temperatura = temperatura;
    }
    public double getValorBase(){
        return valorBase;
    }
    public void setValorBase(double valorBase){
        this.valorBase = valorBase;
    }
    
    public double calcularCostoDeAlmacenamiento(){
        return 0;
    }
    
    public boolean crearProducto(){
        String peticion = "INSERT INTO productos (Nombre,Id,Temperatura,ValorBase)" + 
                "VALUES ('" + nombre + "', '" + id + "', "+ temperatura +","+ valorBase + ")" ;
        ConexionBD conn = new ConexionBD();
        
        if(conn.setAutoCommitBD(false)){
            if(conn.insertarBD(peticion)){
                conn.commitBD();
                conn.cerrarConexion();
                return true;
            }else{
                conn.rollbackBD();
                conn.cerrarConexion();
                return false;
            }
        }else{
            conn.cerrarConexion();
            return false;
        }
    }
    public boolean eliminarProdutos(){
        String peticion = "DELETE FROM productos WHERE id = '"+ id + "'";
        ConexionBD conn = new ConexionBD();
        
        if (conn.setAutoCommitBD(false)){
            if(conn.borrarBD(peticion)){
                conn.commitBD();
                conn.cerrarConexion();
                return true;
            }else{
                conn.rollbackBD();
                conn.cerrarConexion();
                return false;
            }
        }else{
            conn.cerrarConexion();
            return false;
        }
    }
    
    public List<Producto> leerProductos(){
        List<Producto> productos = new ArrayList<>();
        
        String consulta = "SELECT * FROM productos";
        ConexionBD conn = new ConexionBD();
        
        ResultSet result = conn.consultarBD(consulta);
        
        try {
            while (result.next()){
                Producto p = new Producto();             
                p.setNombre(result.getString("nombre"));
                p.setId(result.getString("id"));
                p.setTemperatura(result.getDouble("Temperatura"));
                p.setValorBase(result.getDouble("ValorBase"));
                //usuario.calcularCostoDeAlmacenamiento(result.getDouble());                
                productos.add(p);
            }
        } catch (SQLException e) {
            System.err.println("Error BD" + e.getMessage());
        }
        
        conn.cerrarConexion();
        return productos;
    }
    
    public boolean actualizarProductos(){
        String peticion = "UPDATE productos SET Nombre = '"+ nombre
                +"', Temperatura = " + temperatura
                +", ValorBase = "+ valorBase
                +" WHERE id = '"+ id + "'";
        ConexionBD conn = new ConexionBD();
        
        if (conn.setAutoCommitBD(false)){
            if(conn.actualizarBD(peticion)){
                conn.commitBD();
                conn.cerrarConexion();
                return true;
            }else{
                conn.rollbackBD();
                conn.cerrarConexion();
                return false;
            }
        }else{
            conn.cerrarConexion();
            return false;
        }
    }
 }
